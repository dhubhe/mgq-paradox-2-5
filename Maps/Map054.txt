﻿


EVENT   1
 PAGE   1
  ShowMessageFace("",0,0,2,1)
  ShowMessage("\\n<村人>This is the assembly hall for the village. The village elder can usually be found here.")
  0()



EVENT   2
 PAGE   1
  281(1)
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x09,0x4d,0x6f,0x76,0x65,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  TeleportPlayer(0,53,47,22,0,0)
  216(0)
  0()



EVENT   3
 PAGE   1
  281(1)
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x09,0x4d,0x6f,0x76,0x65,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  TeleportPlayer(0,53,24,24,0,0)
  0()



EVENT   4
 PAGE   1
  281(1)
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x09,0x4d,0x6f,0x76,0x65,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  TeleportPlayer(0,53,11,32,0,0)
  0()



EVENT   5
 PAGE   1
  281(1)
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x09,0x4d,0x6f,0x76,0x65,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  TeleportPlayer(0,53,50,31,0,0)
  0()



EVENT   6
 PAGE   1
  281(1)
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x09,0x4d,0x6f,0x76,0x65,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  TeleportPlayer(0,53,30,44,0,0)
  0()



EVENT   7
 PAGE   1
  281(1)
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x09,0x4d,0x6f,0x76,0x65,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  TeleportPlayer(0,53,48,40,0,0)
  0()



EVENT   8
 PAGE   1
  ShowMessageFace("",0,0,2,1)
  ShowMessage("\\n<店主>ふむ……何の用だね？")
  Shop(1,128,0,0,false)
  605(1,243,0,0)
  605(1,268,0,0)
  605(2,12,0,0)
  605(2,136,0,0)
  605(2,189,0,0)
  605(2,212,0,0)
  605(2,248,0,0)
  0()



EVENT   9
 PAGE   1
  ShowMessageFace("kappa_fc1",0,0,2,1)
  ShowMessage("\\n<河童の鍛冶屋>かっぱっぱ～♪")
  ShowMessage("河童の鍛冶屋だよ～♪")
  355("call_synthesize(15)")
  0()



EVENT   10
 PAGE   1
  ShowMessageFace("",0,0,2,1)
  ShowMessage("\\n<ノブナガ>汝、道具の準備を怠るなかれ……")
  Shop(0,2,0,0,false)
  605(0,13,0,0)
  605(0,17,0,0)
  605(0,18,0,0)
  605(0,20,0,0)
  605(0,102,0,0)
  605(2,1051,0,0)
  605(2,1143,0,0)
  605(2,1148,0,0)
  605(2,1163,0,0)
  605(2,1164,0,0)
  605(2,1721,0,0)
  0()



EVENT   11
 PAGE   1
  ShowMessageFace("",0,0,2,1)
  ShowMessage("\\n<店員>お食事処へようこそ！")
  ShowMessage("ご注文は何になさいますか？")
  Shop(0,361,0,0,false)
  605(0,408,0,0)
  605(0,410,0,0)
  605(0,411,0,0)
  0()



EVENT   12
 PAGE   1
  ShowMessageFace("",0,0,2,1)
  ShowMessage("\\n<野菜売りの少女>やさい、いりませんかー？")
  Shop(0,308,0,0,false)
  605(0,310,0,0)
  605(0,312,0,0)
  0()



EVENT   13
 PAGE   1
  281(1)
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x09,0x4d,0x6f,0x76,0x65,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  TeleportPlayer(0,53,31,18,0,0)
  0()



EVENT   14
 PAGE   1
  0()



EVENT   15
 PAGE   1
  If(1,826,0,2,1)
   If(0,1575,1)
    ShowMessageFace("mob_samurai_fc1",0,0,2,1)
    ShowMessage("\\n<弥生>私も冒険に連れて行ってくれないか？")
    ShowMessage("なんでもいいから斬りたくなってな……")
    ShowChoices(strings("仲間にする","仲間にしない"),0)
    IfPlayerPicksChoice(0,null)
     ShowMessageFace("mob_samurai_fc1",0,0,2,2)
     ShowMessage("\\n<弥生>そうか！　さあ、斬ろう！")
     PictureClear(5)
     205(0,bytes(0x04,0x08,0x6f,0x3a,0x13,0x52,0x50,0x47,0x3a,0x3a,0x4d,0x6f,0x76,0x65,0x52,0x6f,0x75,0x74,0x65,0x09,0x3a,0x0c,0x40,0x72,0x65,0x70,0x65,0x61,0x74,0x46,0x3a,0x0f,0x40,0x73,0x6b,0x69,0x70,0x70,0x61,0x62,0x6c,0x65,0x46,0x3a,0x0a,0x40,0x77,0x61,0x69,0x74,0x54,0x3a,0x0a,0x40,0x6c,0x69,0x73,0x74,0x5b,0x09,0x6f,0x3a,0x15,0x52,0x50,0x47,0x3a,0x3a,0x4d,0x6f,0x76,0x65,0x43,0x6f,0x6d,0x6d,0x61,0x6e,0x64,0x07,0x3a,0x0a,0x40,0x63,0x6f,0x64,0x65,0x69,0x2a,0x3a,0x10,0x40,0x70,0x61,0x72,0x61,0x6d,0x65,0x74,0x65,0x72,0x73,0x5b,0x00,0x6f,0x3b,0x0a,0x07,0x3b,0x0b,0x69,0x0f,0x3b,0x0c,0x5b,0x00,0x6f,0x3b,0x0a,0x07,0x3b,0x0b,0x69,0x2c,0x3b,0x0c,0x5b,0x00,0x6f,0x3b,0x0a,0x07,0x3b,0x0b,0x69,0x00,0x3b,0x0c,0x5b,0x00))
     355("add_actor_ex(575)")
     249(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x4d,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0c,0x66,0x61,0x6e,0x66,0x61,0x6c,0x65,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x69))
     ShowMessageFace("",0,0,2,3)
     ShowMessage("弥生が仲間に加わった！")
     Wait(60)
     EndEventProcessing()
     0()
    IfPlayerPicksChoice(1,null)
     ShowMessageFace("mob_samurai_fc1",1,0,2,4)
     ShowMessage("\\n<弥生>ダメか……")
     0()
    404()
    0()
   EndIf()
   0()
  EndIf()
  RunCommonEvent(7251)
  0()
 PAGE   2
  // condition: party contains actor #575
  0()



EVENT   16
 PAGE   1
  If(1,1055,0,0,0)
   ShowMessageFace("",0,0,2,1)
   ShowMessage("\\n<Village Elder>Oooh, this is bad.\nReal bad...")
   ShowMessageFace("",0,0,2,2)
   ShowMessage("\\n<Village Elder>No, don't mind me. This is our problem, not something you travelers need concern yourselves with.")
   EndEventProcessing()
   0()
  EndIf()
  If(1,1055,0,1,0)
   ChangeVariable(1055,1055,0,0,2)
   ShowMessageFace("",0,0,2,3)
   ShowMessage("\\n<Village Elder>Oh, you're here! You came from Grand Noah to help us, right?")
   ShowMessageFace("durahan_fc1",0,0,2,4)
   ShowMessage("\\n<アーサー>That is correct. We are here in response to your request. So, just what is going on here?")
   If(4,203,0)
    ShowMessageFace("tatunoko_k_fc1",1,0,2,5)
    ShowMessage("\\n<たつこ>We'll solve it!")
    0()
   EndIf()
   ShowMessageFace("",0,0,2,6)
   ShowMessage("\\n<Village Elder>There are four major shrines in this village: the kitsune shrine, tanuki shrine, cat shrine, and snake shrine.")
   ShowMessageFace("",0,0,2,7)
   ShowMessage("\\n<Village Elder>The gods worshiped at each of the shrines have gone mad. One after the other, terrible things have happened at each shrine for no discernible reason...")
   If(4,201,0)
    ShowMessageFace("ashel_fc1",0,0,2,8)
    ShowMessage("\\n<アシェル>Sometimes when it rains, it pours, but four disasters at the same time sounds extreme.")
    0()
   EndIf()
   If(0,4,0)
    ShowMessageFace("alice_fc5",0,0,2,9)
    ShowMessage("\\n<アリス>We get the idea. Can you summarize what happened at each shrine for us?")
    0()
   EndIf()
   If(0,5,0)
    ShowMessageFace("iriasu_fc4",2,0,2,10)
    ShowMessage("\\n<イリアス>So, four separate disasters?\nLet's go through them one by one to start with.")
    0()
   EndIf()
   ShowMessageFace("",0,0,2,11)
   ShowMessage("\\n<Village Elder>First, the Shirohebi of the snake shrine are gathering followers and forming an army of youkai.")
   ShowMessageFace("",0,0,2,12)
   ShowMessage("\\n<Village Elder>It appears that they're massing strength in order to take over the village.  That's what it seems like they've been preparing to do, at least.")
   ShowMessageFace("ruka_fc1",0,0,2,13)
   ShowMessage("\\n<ルカ>A whole army of youkai? We can't let them attack the village!")
   If(0,4,0)
    ShowMessageFace("alice_fc5",0,0,2,14)
    ShowMessage("\\n<アリス>The Shirohebi are a pair of sisters, I believe, with the older one known for her bad behavior. We'll have to knock some sense into her to stop her from doing anything barbaric.")
    0()
   EndIf()
   If(0,5,0)
    ShowMessageFace("iriasu_fc4",2,0,2,15)
    ShowMessage("\\n<イリアス>If I recall correctly, the Shirohebi are a pair of sisters, and the older one is especially evil. We should take this opportunity to rid the world of her.")
    0()
   EndIf()
   If(4,129,0)
    ShowMessageFace("brunhild_fc1",0,0,2,16)
    ShowMessage("\\n<ヒルデ>Mission acknowledged. Eliminate youkai army led by Shirohebi.")
    0()
   EndIf()
   If(0,6,0)
    ShowMessageFace("sonia_fc1",3,0,2,17)
    ShowMessage("\\n<ソニア>Let's bust up that snake shrine! Night raid, morning assault, any time works for me!")
    ShowMessageFace("ruka_fc1",0,0,2,18)
    ShowMessage("\\n<ルカ>(She's really pumped up for this...)")
    0()
   EndIf()
   ShowMessageFace("",0,0,2,19)
   ShowMessage("\\n<Village Elder>Next up, the master of the kitsune shrine is depressed for some reason, and it's spreading unrest among all the kitsune.")
   If(0,4,0)
    ShowMessageFace("alice_fc5",0,0,2,20)
    ShowMessage("\\n<アリス>Tamamo is the master of that shrine, is she not? What could possibly cause her to be depressed?")
    0()
   EndIf()
   If(0,5,0)
    ShowMessageFace("iriasu_fc4",2,0,2,21)
    ShowMessage("\\n<イリアス>The master of the kitsune shrine is that infuriating Tamamo. Anything that could send her into depression bodes very ill.")
    0()
   EndIf()
   If(4,102,0)
    ShowMessageFace("youko_fc1",2,0,2,22)
    ShowMessage("\\n<きつね>I wonder what happened to Tamamo...")
    0()
   EndIf()
   If(4,103,0)
    ShowMessageFace("kamuro_fc1",2,0,2,23)
    ShowMessage("\\n<かむろ>I'm really worried for Tamamo now...")
    0()
   EndIf()
   ShowMessageFace("",0,0,2,24)
   ShowMessage("\\n<Village Elder>The tanuki god can't be found anywhere at the tanuki shrine. Instead, there's a kitsune crying there for some reason.")
   If(0,6,0)
    ShowMessageFace("sonia_fc2",2,0,2,25)
    ShowMessage("\\n<ソニア>A kitsune crying and a tanuki missing? It sounds like the two can't possibly be unrelated.")
    0()
   Else()
    ShowMessageFace("ruka_fc1",0,0,2,26)
    ShowMessage("\\n<ルカ>A kitsune crying and a tanuki missing? It sounds like the two can't possibly be unrelated.")
    0()
   EndIf()
   If(4,53,0)
    ShowMessageFace("slime_fc1",2,0,2,27)
    ShowMessage("\\n<ライム>Maybe someone was bullying them...?")
    0()
   EndIf()
   ShowMessageFace("",0,0,2,28)
   ShowMessage("\\n<Village Elder>At the cat shrine... Well, just go see for yourself. We have no idea which of them is the cat god.")
   If(0,6,0)
    ShowMessageFace("sonia_fc2",2,0,2,29)
    ShowMessage("\\n<ソニア>Which of them...? What does that mean?")
    0()
   Else()
    ShowMessageFace("ruka_fc1",0,0,2,30)
    ShowMessage("\\n<ルカ>What in the world is going on there...")
    0()
   EndIf()
   If(4,222,0)
    ShowMessageFace("nekomata_fc1",4,0,2,31)
    ShowMessage("\\n<たま>...mrrrow?")
    0()
   EndIf()
   ShowMessageFace("durahan_fc1",0,0,2,32)
   ShowMessage("\\n<アーサー>I think we know enough for now. Shall we go investigate the shrines themselves now?")
   ShowMessageFace("",0,0,2,33)
   ShowMessage("\\n<Village Elder>Please, you would be doing us a great service. The hill leading to the four shrines is on the outskirts of the village to the northeast of here.")
   ShowMessageFace("",0,0,2,34)
   ShowMessage("\\n<Village Elder>If you resolve everything, I'll reward you with the [Eastern Book of Secrets]. May fortune smile upon you.")
   If(4,218,0)
    ShowMessageFace("nyumaou_fc1",0,0,2,35)
    ShowMessage("\\n<牛魔王>Sure, you can count on me!")
    0()
   EndIf()
   If(4,79,0)
    ShowMessageFace("nuruko_fc1",0,0,2,36)
    ShowMessage("\\n<ヌルコ>Kyuu!")
    0()
   EndIf()
   EndEventProcessing()
   0()
  EndIf()
  If(1,1055,0,2,0)
   ShowMessageFace("",0,0,2,37)
   ShowMessage("\\n<Village Elder>Please, you would be doing us a great service. The hill leading to the four shrines is on the outskirts of the village to the northeast of here.")
   ShowMessageFace("",0,0,2,38)
   ShowMessage("\\n<Village Elder>If you resolve everything, I'll reward you with the [Eastern Book of Secrets]. May fortune smile upon you.")
   EndEventProcessing()
   0()
  EndIf()
  If(1,1055,0,3,0)
   ChangeVariable(1055,1055,0,0,4)
   241(bytes(0x04,0x08,0x6f,0x3a,0x0d,0x52,0x50,0x47,0x3a,0x3a,0x42,0x47,0x4d,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x14,0x73,0x63,0x65,0x6e,0x65,0x5f,0x65,0x70,0x69,0x6c,0x6f,0x67,0x75,0x65,0x32,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x69,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69))
   355("gain_medal(35)")
   ShowMessageFace("durahan_fc1",1,0,2,39)
   ShowMessage("\\n<アーサー>Elder, we have finished solving the issues with the shrines.")
   ShowMessageFace("",0,0,2,40)
   ShowMessage("\\n<Village Elder>Thank you so much!\nPlease accept this as a token of my gratitude.")
   250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
   ChangeInventory_Item(603,0,0,1)
   If(4,202,0)
    ShowMessageFace("bonny_fc1",1,0,2,41)
    ShowMessage("\\n<ボニー>Yes! Booty!")
    0()
   EndIf()
   If(0,4,0)
    ShowMessageFace("alice_fc5",1,0,2,42)
    ShowMessage("\\n<アリス>This will let us change jobs to Samurai, Taoist, and Ninja. Thanks, it's much appreciated.")
    0()
   EndIf()
   If(0,5,0)
    ShowMessageFace("iriasu_fc4",0,0,2,43)
    ShowMessage("\\n<イリアス>This will allow us to change jobs to Samurai, Taoist, and Ninja. It is received with thanks.")
    0()
   EndIf()
   ShowMessageFace("",0,0,2,44)
   ShowMessage("\\n<Village Elder>I can't thank you enough for your help. You're welcome in this village whenever you want to drop by.")
   If(0,6,0)
    ShowMessageFace("sonia_fc1",0,0,2,45)
    ShowMessage("\\n<ソニア>Great, that's great... right?")
    0()
   Else()
    ShowMessageFace("ruka_fc1",0,0,2,46)
    ShowMessage("\\n<ルカ>Well, we've finished what we came here to do.")
    0()
   EndIf()
   ShowMessageFace("durahan_fc1",1,0,2,47)
   ShowMessage("\\n<アーサー>I owe you my thanks as well. I am truly in your debt.")
   ShowMessageFace("durahan_fc1",1,0,2,48)
   ShowMessage("\\n<アーサー>Actually, Her Majesty the Queen gave me another mission. I am to accompany you even after we've completed our duty in Yamatai.")
   ShowMessageFace("durahan_fc1",1,0,2,49)
   ShowMessage("\\n<アーサー>Therefore, you can continue to count on me. I hope my sword will be useful in your quest to save the world.")
   249(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x4d,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0c,0x66,0x61,0x6e,0x66,0x61,0x6c,0x65,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x69))
   ShowMessageFace("",0,0,2,50)
   ShowMessage("Arthur formally joins the party!")
   If(0,6,0)
    ShowMessageFace("sonia_fc1",0,0,2,51)
    ShowMessage("\\n<ソニア>It's reassuring to know that the Queen of Grand Noah is secretly supporting us.")
    0()
   Else()
    ShowMessageFace("ruka_fc1",0,0,2,52)
    ShowMessage("\\n<ルカ>It's very helpful to have the Queen of Grand Noah secretly supporting us.")
    0()
   EndIf()
   If(0,4,0)
    ShowMessageFace("alice_fc5",2,0,2,53)
    ShowMessage("\\n<アリス>But that's also a sign of how important our responsibility is. Never forget that we can't afford to relax and take it easy.")
    0()
   EndIf()
   If(0,5,0)
    ShowMessageFace("iriasu_fc4",2,0,2,54)
    ShowMessage("\\n<イリアス>Let that remind you of just how important our mission is.")
    0()
   EndIf()
   ShowMessageFace("ruka_fc1",0,0,2,55)
   ShowMessage("\\n<ルカ>Of course. I know how much weight we carry on our shoulders. With that said, let's get going!")
   If(4,62,0)
    ShowMessageFace("gob_fc1",0,0,2,56)
    ShowMessage("\\n<ゴブ>If we do get a chance to relax, let's come back to Yamatai!")
    0()
   EndIf()
   241(bytes(0x04,0x08,0x6f,0x3a,0x0d,0x52,0x50,0x47,0x3a,0x3a,0x42,0x47,0x4d,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x6d,0x75,0x72,0x61,0x37,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x69,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69))
   EndEventProcessing()
   0()
  EndIf()
  If(1,1055,0,4,0)
   ShowMessageFace("",0,0,2,57)
   ShowMessage("\\n<Village Elder>There are no major issues facing the village at the moment. The young lads' worldly desires assembly is nothing new...")
   ShowMessageFace("",0,0,2,58)
   ShowMessage("\\n<Village Elder>Besides, the world would be in trouble if the youth gave up on their sexual desires. Without more children, the village would die off.")
   If(4,382,0)
    ShowMessageFace("morrigan_fc1",0,0,2,59)
    ShowMessage("\\n<モリガン>Sounds about right to me.")
    0()
   EndIf()
   ShowMessageFace("",0,0,2,60)
   ShowMessage("\\n<Village Elder>Get married. Reproduce. Consume. Sleep.\nDon't think. Obey.")
   If(0,4,0)
    ShowMessageFace("alice_fc5",6,0,2,61)
    ShowMessage("\\n<アリス>What the!? Did he just receive some radio waves!?")
    0()
   EndIf()
   If(0,5,0)
    ShowMessageFace("iriasu_fc4",5,0,2,62)
    ShowMessage("\\n<イリアス>He just blurted out his real feelings! Did he have a nervous breakdown!?")
    0()
   EndIf()
   If(12,"exist_party_persona_id?(241)")
    ShowMessageFace("a_emp_fc1",0,0,2,63)
    ShowMessage("\\n<プリエステス>He's probably worn out.")
    0()
   EndIf()
   If(12,"exist_party_persona_id?(242)")
    ShowMessageFace("a_emp_fc1",4,0,2,64)
    ShowMessage("\\n<プリエステス>Fufu.")
    0()
   EndIf()
   If(0,2183,0)
    If(0,2283,1)
     If(0,2228,0)
      If(0,2229,0)
       If(0,2230,0)
        If(0,2231,0)
         ChangeSwitch(2283,2283,0)
         ShowMessageFace("",0,0,2,65)
         ShowMessage("\\n<Village Elder>I've heard that you managed to find all four of the young lads. In reward for your efforts, take this...")
         250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
         ChangeInventory_Item(546,0,0,1)
         ShowMessageFace("",0,0,2,66)
         ShowMessage("\\n<Village Elder>ＯＢＥＹ")
         If(0,4,0)
          ShowMessageFace("alice_fc5",2,0,2,67)
          ShowMessage("\\n<アリス>Perhaps you should go lie down for a bit?")
          0()
         EndIf()
         If(0,5,0)
          ShowMessageFace("iriasu_fc4",2,0,2,68)
          ShowMessage("\\n<イリアス>Get some rest...")
          0()
         EndIf()
         EndEventProcessing()
         0()
        EndIf()
        0()
       EndIf()
       0()
      EndIf()
      0()
     EndIf()
     ShowMessageFace("",0,0,2,69)
     ShowMessage("\\n<Village Elder>Oh ho ho, this is an exciting chance!\nFind the young lads scattered throughout the world!")
     ShowMessageFace("",0,0,2,70)
     ShowMessage("\\n<Village Elder>If you find all four, you get an amazing prize! It's seeking time!!")
     0()
    EndIf()
    0()
   EndIf()
   EndEventProcessing()
   0()
  EndIf()
  0()



EVENT   17
 PAGE   1
  ShowMessageFace("kooni_fc1",0,0,2,1)
  ShowMessage("\\n<小鬼>「東方秘伝書」があれば、亜人種は「夜叉」に転種できるの。")
  ShowMessage("刀技を使いこなす、強力な鬼だよ！")
  0()



EVENT   18
 PAGE   1
  ShowMessageFace("youki_fc1",0,0,2,1)
  ShowMessage("\\n<妖鬼>職業「狂戦士」って知ってるか？")
  ShowMessage("斧を振り回して暴れ回る、文字通り狂った戦士だ。")
  ShowMessageFace("youki_fc1",0,0,2,2)
  ShowMessage("\\n<妖鬼>戦闘中、いっさいの命令を受け付けないが……")
  ShowMessage("そのパワーは、圧倒的だとさ。")
  ShowMessageFace("youki_fc1",0,0,2,3)
  ShowMessage("\\n<妖鬼>通常攻撃で溜まるSPの量が多くなるアビリティも、魅力らしい。")
  ShowMessage("戦技スキルが得意な奴は、狂戦士を経験させてみたらどうだ？")
  0()



EVENT   19
 PAGE   1
  ShowMessageFace("",0,0,2,1)
  ShowMessage("\\n<店員>鬼ってのは、よく食うな……")
  ShowMessage("また買い出しに行かなくちゃ。")
  355("actor_label_jump")
  EndEventProcessing()
  DefineLabel("164")
  ShowMessageFace("kooni_fc1",0,0,2,2)
  ShowMessage("\\n<雫>小鬼だって、いっぱい食べるよ～！")
  EndEventProcessing()
  DefineLabel("300")
  ShowMessageFace("youki_fc1",0,0,2,3)
  ShowMessage("\\n<くれは>そりゃ、鬼だもんな！")
  EndEventProcessing()
  0()



EVENT   20
 PAGE   1
  ShowMessageFace("",0,0,2,1)
  ShowMessage("\\n<店員>こっちは炊事場です。")
  ShowMessage("注文は、お座敷の方でどうぞ～。")
  0()



EVENT   21
 PAGE   1
  ShowMessageFace("",0,0,2,1)
  ShowMessage("\\n<老婆>こんな遠くの村まで、よくぞ来なさった。")
  ShowMessage("5Gで泊まっていくかね？\\$")
  ShowChoices(strings("はい","いいえ"),2)
  IfPlayerPicksChoice(0,null)
   If(7,5,0)
    125(1,0,5)
    221()
    249(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x4d,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x08,0x49,0x6e,0x6e,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x69))
    Wait(150)
    314(0,0)
    222()
    0()
   Else()
    ShowMessageFace("",0,0,2,2)
    ShowMessage("\\n<老婆>文無しか……さっさと失せな。")
    0()
   EndIf()
   0()
  IfPlayerPicksChoice(1,null)
   0()
  404()
  0()



EVENT   22
 PAGE   1
  ShowMessageFace("",0,0,2,1)
  ShowMessage("\\n<衛兵>職業「侍」は、力だけじゃなく器用さも重要だ。")
  ShowMessage("スキル「刀技」の威力も、器用さが関わってくるぞ。")
  ShowMessageFace("",0,0,2,2)
  ShowMessage("\\n<衛兵>それゆえ、パワー一辺倒の者には向かない職業だな。")
  ShowMessage("器用さが高い者なら、存分に強さを発揮するだろう。")
  0()



EVENT   23
 PAGE   1
  ShowMessageFace("",0,0,2,1)
  ShowMessage("\\n<忍者>忍者には器用さが重要……")
  ShowMessage("忍びの術にも、器用さが深く関わるのだ。")
  0()



EVENT   24
 PAGE   1
  If(1,825,0,2,1)
   If(0,1574,1)
    ShowMessageFace("mob_miko1_fc1",1,0,2,1)
    ShowMessage("\\n<さつき>あの、少しばかり旅をしたいんです。")
    ShowMessage("私も連れて行ってくれませんか……？")
    ShowChoices(strings("仲間にする","仲間にしない"),0)
    IfPlayerPicksChoice(0,null)
     ShowMessageFace("mob_miko1_fc1",6,0,2,2)
     ShowMessage("\\n<さつき>うふふっ、楽しみです……♪")
     PictureClear(5)
     205(0,bytes(0x04,0x08,0x6f,0x3a,0x13,0x52,0x50,0x47,0x3a,0x3a,0x4d,0x6f,0x76,0x65,0x52,0x6f,0x75,0x74,0x65,0x09,0x3a,0x0c,0x40,0x72,0x65,0x70,0x65,0x61,0x74,0x46,0x3a,0x0f,0x40,0x73,0x6b,0x69,0x70,0x70,0x61,0x62,0x6c,0x65,0x46,0x3a,0x0a,0x40,0x77,0x61,0x69,0x74,0x54,0x3a,0x0a,0x40,0x6c,0x69,0x73,0x74,0x5b,0x09,0x6f,0x3a,0x15,0x52,0x50,0x47,0x3a,0x3a,0x4d,0x6f,0x76,0x65,0x43,0x6f,0x6d,0x6d,0x61,0x6e,0x64,0x07,0x3a,0x0a,0x40,0x63,0x6f,0x64,0x65,0x69,0x2a,0x3a,0x10,0x40,0x70,0x61,0x72,0x61,0x6d,0x65,0x74,0x65,0x72,0x73,0x5b,0x00,0x6f,0x3b,0x0a,0x07,0x3b,0x0b,0x69,0x0f,0x3b,0x0c,0x5b,0x00,0x6f,0x3b,0x0a,0x07,0x3b,0x0b,0x69,0x2c,0x3b,0x0c,0x5b,0x00,0x6f,0x3b,0x0a,0x07,0x3b,0x0b,0x69,0x00,0x3b,0x0c,0x5b,0x00))
     355("add_actor_ex(574)")
     249(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x4d,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0c,0x66,0x61,0x6e,0x66,0x61,0x6c,0x65,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x69))
     ShowMessageFace("",0,0,2,3)
     ShowMessage("さつきが仲間に加わった！")
     Wait(60)
     EndEventProcessing()
     0()
    IfPlayerPicksChoice(1,null)
     ShowMessageFace("mob_miko1_fc1",0,0,2,4)
     ShowMessage("\\n<さつき>そうですか……")
     ShowMessage("残念ですけど、仕方ありませんね。")
     0()
    404()
    0()
   EndIf()
   0()
  EndIf()
  RunCommonEvent(7241)
  0()
 PAGE   2
  // condition: party contains actor #574
  0()



EVENT   25
 PAGE   1
  ShowMessageFace("",0,0,2,1)
  ShowMessage("\\n<村人>オロチの洞には、隠し宝物庫があるという。")
  ShowMessage("行き方を記したメモを入手したんだが、字が掠れてほとんど読めなくてね……")
  ShowMessageFace("",0,0,2,2)
  ShowMessage("\\n<村人>かろうじて、判読できた箇所だと……")
  ShowMessage("「ぴょんと飛んで～」「縄を降り～」くらいかね。")
  If(0,2149,0)
   ShowMessageFace("bonny_fc1",1,0,2,3)
   ShowMessage("\\n<ボニー>宝物庫への行き方を記したメッセージ！")
   ShowMessage("海賊魂が燃え上がるぞ！")
   ShowMessageFace("ashel_fc1",0,0,2,4)
   ShowMessage("\\n<アシェル>ジャンプして、縄を降りる……")
   ShowMessage("覚えておいた方がよさそうだね。")
   0()
  EndIf()
  0()



EVENT   26
 PAGE   1
  ShowMessageFace("hutakuti_fc1",1,0,2,1)
  ShowMessage("\\n<二口娘>お茶もおだんごも、とっても美味しいです……♪")
  355("actor_label_jump")
  EndEventProcessing()
  DefineLabel("5")
  ShowMessageFace("alice_fc5",7,0,2,2)
  ShowMessage("\\n<アリス>おいしそう……♪")
  ShowMessage("ルカ、余にも買って！")
  EndEventProcessing()
  DefineLabel("26")
  ShowMessageFace("iriasu_fc4",1,0,2,3)
  ShowMessage("\\n<イリアス>おいしそうです……♪")
  ShowMessage("ルカ、私にも買って下さい！")
  EndEventProcessing()
  0()



EVENT   27
 PAGE   1
  ShowMessageFace("hutakuti_fc1",6,0,2,1)
  ShowMessage("\\n<二口娘>後ろの口が、勝手におだんごを～！")
  ShowMessage("きゃぁぁぁ～～！！")
  355("actor_label_jump")
  EndEventProcessing()
  DefineLabel("75")
  ShowMessageFace("hituzi_fc1",7,0,2,2)
  ShowMessage("\\n<メリー>前の口が、勝手にお酒を～！")
  ShowMessage("メェェェェェ～！！")
  EndEventProcessing()
  0()



EVENT   28
 PAGE   1
  If(0,2317,1)
   ShowMessageFace("",0,0,2,1)
   ShowMessage("\\n<村人>オロチの洞に、ヤマタノオロチという大妖怪が棲んでいるのですが……")
   ShowMessage("生け贄を出すよう、この村にしょっちゅう脅しをかけてくるのです。")
   ShowMessageFace("",0,0,2,2)
   ShowMessage("\\n<村人>若衆達が自発的かつ勝手に生け贄に行くので、実害はありませんが……")
   ShowMessage("危険な大妖怪が村の側に棲んでいるのは、やはり不安ですね。")
   If(0,4,0)
    ShowMessageFace("alice_fc5",3,0,2,3)
    ShowMessage("\\n<アリス>放置すべきでないが、ヤマタノオロチは非常に強力な妖魔。")
    ShowMessage("今の我々の手に余るようなら、討伐を先送りにすべきだな……")
    0()
   EndIf()
   If(0,5,0)
    ShowMessageFace("iriasu_fc4",2,0,2,4)
    ShowMessage("\\n<イリアス>ヤマタノオロチなど、ただちに討ち滅ぼすべきですが……")
    ShowMessage("今の我々では難しいかもしれませんね、先送りにしましょう。")
    0()
   EndIf()
   0()
  Else()
   ShowMessageFace("",0,0,2,5)
   ShowMessage("\\n<村人>ヤマタノオロチが、旅の戦士に退治されたという話です。")
   ShowMessage("元より実害は少なかったですが、不安はすっかり解消されました。")
   0()
  EndIf()
  0()



EVENT   29
 PAGE   1
  ShowMessageFace("",0,0,2,1)
  ShowMessage("\\n<若衆>干涸らびちゃうよぉ……")
  0()



EVENT   30
 PAGE   1
  ShowMessageFace("",0,0,2,1)
  ShowMessage("\\n<若衆>あへぇぇぇ……")
  ShowMessage("ヤマタノオロチさまぁ……")
  0()



EVENT   31
 PAGE   1
  ShowMessageFace("",0,0,2,1)
  ShowMessage("\\n<娘>ヤマタノオロチの生け贄になった人も、一年もすれば戻ってくるんですが……")
  ShowMessage("しばらくは、このザマです。")
  ShowMessageFace("",0,0,2,2)
  ShowMessage("\\n<娘>最初のうちは、身を挺して村を救った英雄という事で、")
  ShowMessage("丁重に看護されていたんですが……")
  ShowMessageFace("",0,0,2,3)
  ShowMessage("\\n<娘>次から次へと生け贄志願の若衆がなだれ込んでいく昨今、")
  ShowMessage("出戻り組はそこらに転がしておく程度の扱いになりました。")
  355("actor_label_jump")
  EndEventProcessing()
  DefineLabel("163")
  ShowMessageFace("lily_fc1",3,0,2,4)
  ShowMessage("\\n<リリィ>もったいないわね……何かに再利用できないかしら？")
  EndEventProcessing()
  DefineLabel("155")
  ShowMessageFace("saki_fc1",0,0,2,5)
  ShowMessage("\\n<サキ>慰問ライブ、やっちゃおうかな～？")
  EndEventProcessing()
  0()



EVENT   32
 PAGE   1
  ShowMessageFace("",0,0,2,1)
  ShowMessage("\\n<村人>昔ならば、ヤマタノオロチに若衆を奪われれば、困ったところだが……")
  ShowMessage("外国人の働き手も多い昨今、労働力が不足する事もなくなってな。")
  ShowMessageFace("",0,0,2,2)
  ShowMessage("\\n<村人>若衆が自発的に生け贄に行きたいなら、好きにするがよかろう……")
  ShowMessage("……というのが、実際のところよ。")
  0()



EVENT   33
 PAGE   1
  ShowMessageFace("",0,0,2,1)
  ShowMessage("\\n<老人>ヤマタノオロチ様は、生け贄を求めるだけ。")
  ShowMessage("殺生をなさらぬどころか、いざという時は村を守護して下さる方よ。")
  If(1,1055,0,3,2)
   ShowMessageFace("",0,0,2,2)
   ShowMessage("\\n<老人>四大神社の凶事の方が、よほどの大問題じゃ。")
   ShowMessage("ああ、この村はどうなってしまうのか……")
   0()
  EndIf()
  355("actor_label_jump")
  EndEventProcessing()
  DefineLabel("230")
  ShowMessageFace("yamatanooroti_fc1",3,0,2,3)
  ShowMessage("\\n<ヤマタノオロチ>うむ、この村は妾の縄張り……")
  ShowMessage("害をもたらす事などせぬわ。")
  EndEventProcessing()
  0()



EVENT   34
 PAGE   1
  ShowMessageFace("",0,0,2,1)
  ShowMessage("\\n<若衆>ヤマタノオロチさまぁ……もっとぉ……")
  0()



EVENT   35
 PAGE   1
  ShowMessageFace("",0,0,2,1)
  ShowMessage("\\n<若衆>あひーーーーッ！！")
  0()



EVENT   36
 PAGE   1
  ShowMessageFace("",0,0,2,1)
  ShowMessage("\\n<村人>情欲に駆られ、ヤマタノオロチの元に行こうかと思った事もあったけど……")
  ShowMessage("ああして晒し者になる事を考えると、ちょっとなぁ。")
  355("actor_label_jump")
  EndEventProcessing()
  DefineLabel("134")
  ShowMessageFace("chrom_fc2",5,0,2,2)
  ShowMessage("\\n<クロム>惨めな有様じゃのう……")
  EndEventProcessing()
  0()



EVENT   37
 PAGE   1
  ShowMessageFace("",0,0,2,1)
  ShowMessage("\\n<女の子>ママー！　あそこで転がってる人達、なぁに？")
  ShowMessageFace("",0,0,2,2)
  ShowMessage("\\n<母親>あれは、欲望に溺れた馬鹿男の成れの果てだよ。")
  0()



EVENT   38
 PAGE   1
  ShowMessageFace("",0,0,2,1)
  ShowMessage("\\n<女の子>ママー！　あそこで転がってる人達、なぁに？")
  ShowMessageFace("",0,0,2,2)
  ShowMessage("\\n<母親>あれは、欲望に溺れた馬鹿男の成れの果てだよ。")
  0()



EVENT   39
 PAGE   1
  ShowMessageFace("",0,0,2,1)
  ShowMessage("\\n<若衆>ふぁぁぁ……きもちいい……")
  0()



EVENT   40
 PAGE   1
  ShowMessageFace("",0,0,2,1)
  ShowMessage("\\n<娘>ヤマタノオロチの生け贄と言っても……")
  ShowMessage("一年くらい、全身をベロベロ舐め回されるだけでしょ？")
  ShowMessageFace("",0,0,2,2)
  ShowMessage("\\n<娘>どうせ馬鹿男はみんな、エロ心で生け贄になってるんだし……")
  ShowMessage("本人が悦んでるなら、放っておけばいいんじゃない？")
  0()



EVENT   41
 PAGE   1
  ShowMessageFace("",0,0,2,1)
  ShowMessage("\\n<老婆>ヤマタノオロチの件、あたしゃ不安だね……")
  ShowMessage("今は無害っていっても、この先どうなるか分からないじゃないか。")
  ShowMessageFace("",0,0,2,2)
  ShowMessage("\\n<老婆>とんでもなくおっかない大妖怪、って事には違いないんだ。")
  ShowMessage("いつか気が変わって、村を荒らしたりするかもしれないよ。")
  355("actor_label_jump")
  EndEventProcessing()
  DefineLabel("230")
  ShowMessageFace("yamatanooroti_fc1",3,0,2,3)
  ShowMessage("\\n<ヤマタノオロチ>安心せよ、小娘。")
  ShowMessage("妾はこの村に愛着があるのだ。")
  EndEventProcessing()
  0()



EVENT   42
 PAGE   1
  ShowMessageFace("",0,0,2,1)
  ShowMessage("\\n<商人>食べ物、甘味、色々あるよっ！")
  Shop(0,332,0,0,false)
  605(0,333,0,0)
  605(0,334,0,0)
  605(0,335,0,0)
  605(0,336,0,0)
  0()



EVENT   43
 PAGE   1
  ShowMessageFace("nekomata_fc1",0,0,2,1)
  ShowMessage("\\n<ねこまた>……………………")
  ShowMessageFace("",0,0,2,2)
  ShowMessage("ねこまたが魚に忍び寄っている！")
  0()



EVENT   44
 PAGE   1
  ShowMessageFace("hutakuti_fc1",2,0,2,1)
  ShowMessage("\\n<二口娘>このあんこ、おいしそう……")
  ShowMessage("じゅるり……")
  355("actor_label_jump")
  EndEventProcessing()
  DefineLabel("143")
  ShowMessageFace("eva_fc1",0,0,2,2)
  ShowMessage("\\n<エヴァ>頂いちゃおうよ！")
  ShowMessageFace("hutakuti_fc1",2,0,2,3)
  ShowMessage("\\n<二口娘>ああ、悪の誘惑が……")
  EndEventProcessing()
  0()



EVENT   45
 PAGE   1
  ShowMessageFace("yukionna_fc1",2,0,2,1)
  ShowMessage("\\n<雪女>実は、私には秘密があるの……")
  ShowMessage("人間でない事が夫にバレたら、一緒に暮らせない……")
  355("actor_label_jump")
  EndEventProcessing()
  DefineLabel("121")
  ShowMessageFace("page65537_fc1",1,0,2,2)
  ShowMessage("\\n<コーネリア>悲恋ですね、大好物です……")
  EndEventProcessing()
  DefineLabel("221")
  ShowMessageFace("yukionna_fc1",0,0,2,3)
  ShowMessage("\\n<お雪>幸せになりたいわね……")
  EndEventProcessing()
  0()



EVENT   46
 PAGE   1
  ShowMessageFace("",0,0,2,1)
  ShowMessage("\\n<夫>妻は、なぜあれでバレてないと思っているのだろう……")
  ShowMessage("ああ、味噌汁がまた凍り付いている。")
  355("actor_label_jump")
  EndEventProcessing()
  DefineLabel("72")
  ShowMessageFace("phoenix_fc1",1,0,2,2)
  ShowMessage("\\n<ミニ>溶かしてあげるのだ！")
  ShowMessage("ぼわ～っ！！")
  ShowMessageFace("",0,0,2,3)
  ShowMessage("\\n<夫>あつっ！　あぁっ、熱っ！！")
  EndEventProcessing()
  0()



EVENT   47
 PAGE   1
  ShowMessageFace("slime_fc1",0,0,2,1)
  ShowMessage("\\n<スライム娘>ござる～♪　ござる～♪")
  0()



EVENT   48
 PAGE   1
  ShowMessageFace("slime_fc1",0,0,2,1)
  ShowMessage("\\n<スライム娘>この村で、ニンジャを見つけたよ！")
  355("actor_label_jump")
  EndEventProcessing()
  DefineLabel("219")
  ShowMessageFace("kunoitielf_fc1",0,0,2,2)
  ShowMessage("\\n<かすみ>見つかってしまったか……")
  EndEventProcessing()
  DefineLabel("266")
  ShowMessageFace("kunoitisanbi_fc1",0,0,2,3)
  ShowMessage("\\n<ミナモ>そのような忍びは、下の下……")
  EndEventProcessing()
  0()



EVENT   49
 PAGE   1
  If(0,2317,1)
   ShowMessageFace("",0,0,2,1)
   ShowMessage("\\n<戦士>オロチの洞に、ヤマタノオロチ退治に向かったのだが……")
   ShowMessage("一目見て、逃げ出してしまったよ。")
   ShowMessageFace("",0,0,2,2)
   ShowMessage("\\n<戦士>あんなバケモノ、人間に敵うはずがない。")
   ShowMessage("君達も、ヤマタノオロチとは戦わない方がいいぞ。")
   0()
  Else()
   ShowMessageFace("",0,0,2,3)
   ShowMessage("\\n<戦士>まさか、ヤマタノオロチを退治したのか……！？")
   ShowMessage("感服したよ、とんでもない勇者もいたものだな……")
   0()
  EndIf()
  0()



EVENT   50
 PAGE   1
  ShowMessageFace("",0,0,2,1)
  ShowMessage("\\n<老人>河童の鍛冶屋は、実に良い腕前をしとるよ。")
  ShowMessage("クワ一本で、世界を救える気がするほどじゃ。")
  0()



EVENT   51
 PAGE   1
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  ChangeSelfSwitch("A",0)
  ChangeInventory_Item(32,0,0,1)
  0()
 PAGE   2
  // condition: self-switch A is ON
  0()



EVENT   52
 PAGE   1
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  ChangeSelfSwitch("A",0)
  ChangeInventory_Item(56,0,0,1)
  0()
 PAGE   2
  // condition: self-switch A is ON
  0()



EVENT   53
 PAGE   1
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  ChangeSelfSwitch("A",0)
  ChangeInventory_Item(209,0,0,1)
  0()
 PAGE   2
  // condition: self-switch A is ON
  0()



EVENT   54
 PAGE   1
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  ChangeSelfSwitch("A",0)
  ChangeInventory_Item(21,0,0,1)
  0()
 PAGE   2
  // condition: self-switch A is ON
  0()
